let application = new Trunk({
  addVisitor: false,
  addEventStorage: false,
  getForms: {
    state: false,
    forms: [],
    },
  addSocket: {
    state: true,
    lib: io,
    url: 'http://localhost:3000',
    events: [{
      message: 'get forms',
      socketEvent: 'on',
      callback: (msg) =>  {
        buildForms(msg);
        }
      }, {
        message: 'snd',
        socketEvent: 'emit',
        callback: null
            }]
            }
        });



function buildForms(msg) {
  if(document.getElementsByTagName('form').length === 0) {
    JSON.parse(msg).forEach((form) => {
      buildForm(form);
    });
  } else {
    JSON.parse(msg).forEach((form) => {
      updateForm(form);
    });
  }
}


function updateForm(form) {
  form.leafs.forEach((input) => {
    if (input.type !== 'checkbox') {
      document.getElementById(input.id).value = input.value;
    } else {
      document.getElementById(input.id).checked = input.checked;
    }
  });
}


function buildForm(form) {
  let targetDiv = document.getElementById('construct'),
      nform = document.createElement(form.nodeName.toLowerCase());
  nform.id = form.id;
  nform.name = form.name;
  nform.method = form.method;
  
  let counter = 0;
  
  form.leafs.forEach((field) => {
    buildInputs(field).then((r) => {
      nform.appendChild(r);
      if (counter === form.leafs.length-1) {
        targetDiv.appendChild(nform);
      } else {
        counter = counter + 1;
      }
    });
  });
}
//// Build input forms
function buildInputs(obj) {
  return new Promise((resolve, reject)=> {
    let input = document.createElement('input');
        input.id = obj.id;
        input.type = obj.type;
        input.value = obj.value;
        if (obj.type = 'checkbox') {
            input.checked = obj.checked;
        }
        resolve(input);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('form-action-handler').onclick = () => {
    application.addFormStorage({state: true,forms: ['none']}).then(() => {
        application.emit({index:0, reget: false});
      });
  };
});

