// (пер) - зоопарк

// Main object
function shenshengObject() {
  this._id = 'pettyJS-'+ Math.floor(Math.random() * (9999 - 10 + 1)) + 10;
}
shenshengObject.prototype.setAttr = function(wtf, value) {
  this[wtf] = value;
}


// Visitor class
function Visitor() {
  shenshengObject.call(this);
  this.webInfo = undefined;
  this.authInfo = undefined;
}
Visitor.prototype = Object.create(shenshengObject.prototype);
Visitor.prototype.constructor = Visitor;


// Tag Object
function shenshengTag() {
  shenshengObject.call(this);
  this.tagName = undefined;
  this.tagId = undefined;
  this.classes = undefined;
  this.internalElements = undefined;
}
shenshengTag.prototype = Object.create(shenshengObject.prototype);
shenshengTag.prototype.constructor = shenshengTag;


//// Form object
function tagForm() {
  shenshengTag.call(this);
  this.method  = undefined;
  this.fields = undefined;
}
tagForm.prototype = Object.create(shenshengTag.prototype);
tagForm.prototype.constructor = tagForm;


//// Input object
function tagInput() {
  shenshengTag.call(this);
  this.value = undefined;
  this.type = undefined;
}
tagInput.prototype = Object.create(shenshengTag.prototype);
tagInput.prototype.constructor = shenshengTag;


// Listened object
function shenshengListener() {
  shenshengObject.call(this);
  this.event = undefined;
  this.callback = undefined;
}
shenshengListener.prototype = Object.create(shenshengObject.prototype);
shenshengListener.prototype.setListener = function(wtf, trigger) {
  this['event'] = wtf;
  this['callback'] = trigger;
  document.addEventListener(wtf, trigger());
}
shenshengListener.prototype.constructor = shenshengListener;


// Visitor class
function shenshengSocket() {
  shenshengObject.call(this);
  this.connection = undefined;
  this.time = undefined;
  this.events = [];
}
shenshengSocket.prototype = Object.create(shenshengObject.prototype);
shenshengSocket.prototype.constructor = shenshengSocket;


// Shenheng event object
function shenshengEvent() {
  shenshengObject.call(this);
  this.parent = undefined;
}
shenshengEvent.prototype = Object.create(shenshengObject.prototype);
shenshengEvent.prototype.constructor = shenshengEvent;


// Socket event object
function socketEvent() {
  shenshengEvent.call(this);
  this.parent = undefined
  this.eventType = undefined;
  this.event = undefined;
  this.callback = undefined;
}
socketEvent.prototype = Object.create(shenshengEvent.prototype);
socketEvent.prototype.setEvent = function(wtf, trigger) {
  this.setAttr('event', wtf);
  this.setAttr('callback', trigger);
};
socketEvent.prototype.constructor = socketEvent;
