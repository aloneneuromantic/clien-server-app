// GENERIC OBJECT
class GenericObject {
  constructor() {
    this._id = `obj-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
    this.date = new Date();
  }
  get(value) {
    return this[value];
  }
  set(wtf, value) {
    this[wtf] = value;
  }
}



class Tree extends GenericObject {
    constructor() {
        super();
        this._id = `tree-${Math.floor(Math.random() * (999999-10 +1)) + 10}`;
        this.trunks = [];
    }
    pushTrunk(trunk) {
        this.trunks.push(trunk);
    }
}

// Main application OBJECT
class Trunk extends GenericObject {
  constructor(obj) {
    super();

    this._id = `trunk-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
    this.sprouts = [];

    if(obj.addVisitor === true) {
      this.pushSprout(new Visitor(this._id));
    }

    if(obj.addEventStorage.length !== 0 && obj.addEventStorage.length !== undefined) {
      obj.addEventStorage.forEach((listener) => {
        this.pushSprout(new EventStorage(this._id, listener));
      });
    }

    if(obj.addSocket.state === true) {
      this.pushSprout(new Socket(this, obj.addSocket));
    }

    if(obj.getForms.state === true) {
      this.pushSprout(new FormStorage(this._id, obj.getForms));
    }
    
    this.wateringTrunk();
  }
  pushSprout(sprout) {
    this.sprouts.push(sprout);
  }
  // Main API
  wateringTrunk() {
    this.sprouts.forEach((e) => {
      [Visitor,
        Socket,
        EventStorage,
        FormStorage].forEach((c) =>{
          if(e instanceof c) {
            this[`${c.name}`] = e;
          }
        });
    });
  }
  addFormStorage(obj) {
    return new Promise((resolve) => {
        this.pushSprout(new FormStorage(this._id, obj));
        resolve(true);
      });
  }
  getVisitor() {
    return this.Visitor;
  }
  getSocket() {
    return this.Socket;
  }
  getEventStorage() {
    return this.EventStorage;
  }
  getFormsStorage() {
    if (this.FormStorage) {
        return this.FormStorage;
    } else {
      return this.sprouts[1];
    }
  }
  regetFormValues(form) {
    return new Promise((resolve) => {
        let count = 0,
            max = form.leafs.length - 1;
        form.leafs.forEach((input) => {
            input.getValues();
            if (count === max) {
                resolve(true);
            } else {
              count = count + 1;
            }
          });
      });
  }
  regetFormsValues() {
    return new Promise((resolve) => {
        let count = 0,
            max = this.getFormsStorage().getFormsList().length - 1;
        this.getFormsStorage().getFormsList().forEach((form) => {
          this.regetFormValues(form)
          .then(() => {
            if (count === max) {
              resolve(true);
            } else {
              count = count + 1;
            }
          });
        });
      });
  }
  emit(obj) {
    console.log('EMIT');
    if (obj.reget === false) {
        this.Socket.emitList[obj.index].emit(JSON.stringify(this.getFormsStorage().getFormsList()));
    } else {
      this.regetFormsValues().then(() => {
        this.Socket.emitList[obj.index].emit(JSON.stringify(this.getFormsStorage().getFormsList()));
      });
    }
  }
}


// Sprout for application
class Sprout extends GenericObject{
  constructor(trunk) {
    super();
    this._id = this._id = `sprout-${Math.floor(Math.random() * (999999 - 10 + 1))+10}`;
    this._trunk = trunk;
    this.rames = [];
  }
  pushRame(rame) {
    this.rames.push(rame);
  }
}


// Rame for application
class Rame extends Sprout {
  constructor(trunk, sprout) {
    super(trunk);
    this._id = `rame-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
    this._sprout = sprout;
    this.leafs = [];
  }
  pushLeaf(leaf) {
    this.leafs.push(leaf);
  }
}


// Leaf for application
class Leaf extends GenericObject {
  constructor(trunk, sprout, rame) {
    super();
    this._id = `leaf-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
    this._trunk = trunk;
    this._sprout = sprout;
    this._rame = rame;
  }
}


// // Class for dateInfo
class DateInfo extends Rame {
    constructor(trunk, sprout) {
      super(trunk, sprout);
      let date = new Date();
      this.openDate = date;
      this.timezone = date.getTimezoneOffset()/60;
    }
}


// Class for location
class LocationInfo extends Rame {
    constructor(trunk, sprout) {
      super(trunk, sprout);
      ['hash',
        'hostname',
        'href',
        'origin',
        'pathname',
        'port',
        'protocol',
        'search'].forEach((item) => {
        this[item] = location[item];
      });
      this.fullUrl = location.hostname + location.pathname + location.search;
      this.pageCounter = history.length;
      this.refferer = document.refferer;
    }
}


// Browser information
class BrowserInfo extends Leaf {
  constructor(trunk, sprout, rame) {
    super(trunk, sprout, rame);
    let nav = navigator;
    this.detectedName = this.detectBrowserName(nav.userAgent);
    ['appName',
      'appVersion',
      'userAgent',
      'product',
      'language',
      'languages',
      'onLine',
      'platform',
      'cookieEnabled'].forEach((item) => {
      this[item] = nav[item];
    });
    this.javaEnabled = nav.javaEnabled();
    this.cookieFirst = document.cookie;
    this.cookiesSecond = decodeURIComponent(document.cookie.split(";"));
    this.localStorage = localStorage;
  }
  detectBrowserName(ua) {
    if(ua.search(/MSIE/) > -1) return 'InternetExplorer';
    if(ua.search(/Trident/) > -1) return 'InternetExplorer(Trident)';
    if(ua.search(/OPR/) >-1) return 'NewOpera';
    if(ua.search(/Yowser/) > -1 ) return 'YandexBrowser';
    if(ua.search(/UBrowser/) > -1) return 'UCBrowser';
    if(ua.search(/SeaMonkey/) > -1) return 'SeaMonkey';
    if(ua.search(/Iceweasel/) > -1) return 'IceWeasel';
    if(ua.search(/Opera/) > -1) return 'OldOpera';
    if(ua.search(/Firefox/) > -1) return 'Firefox';
    if(ua.search(/Vivaldi/) > -1) return 'Vivaldi';
    if(ua.search(/Edge/) > -1) return 'Edge';
    if(ua.search(/Safari/) > -1 && navigator.vendor.indexOf('Apple') >-1 && ua && ua.match('CriOS')) return 'Safari';
    if(ua.search(/Konqueror/) > -1) return 'Konqueror';
    if(ua.search(/Chrome/) > -1 ) return 'GoogleChrome';

    return 'UnknownBrowser';
  }
}


// Class of screen
class ScreenInfo extends Leaf {
  constructor(trunk, sprout, rame) {
    super(trunk, sprout, rame);
    ['width',
      'height',
      'availWidth',
      'availHeight',
      'colorDepth',
      'pixelDepth'].forEach((item) => {
      this[item] = screen[item];
    });
    ['width','height'].forEach((item) => {
      this[`${item}Document`] = document[item];
    });
    this.innerWidth = innerWidth;
    this.innerHeight = innerHeight;
  }
}


// Class for Device
class DeviceInfo extends Rame {
  constructor(trunk, sprout) {
    super(trunk, sprout);
    let userAgent = navigator.userAgent;
    this.type = this.getDeviceType(userAgent);
    this.os = this.getDeviceOs(userAgent);
    [
      new BrowserInfo(trunk, sprout, this._id),
      new ScreenInfo(trunk, sprout, this._id)].forEach((item) => {
      this.pushLeaf(item);
    });
  }
  getDeviceType(ua) {
      if (ua.match(/iPhone/) ||
            ua.match(/BlackBerry/) ||
            ua.match(/(Windows Phone OS|Windows CE|Windows Mobile)/) ||
            ua.match(/Mobile/) ||
            ua.match(/(Opera Mini|IEMobile|SonyEricsson|smartphone)/)) {
          return 'mobile';
      } else if(ua.match(/iPod/) ||
                  ua.match(/iPad/) ||
                  ua.match(/PlayBook/) ||
                  ua.match(/(GT-P1000|SGH-T849|SHW-M180S)/) ||
                  ua.match(/Tablet PC/) ||
                  ua.match(/(PalmOS|PalmSource| Pre\/)/) ||
                  ua.match(/(Kindle)/)) {
          return 'tablet';
      } else {
          return 'desktop';
      }
  }
  getDeviceOs(ua) {
      if(this.type === 'desktop') {
        if(ua.search(/Windows/) > -1) {
          let tmp = this.ua.toLowerCase();
          //
          if (tmp.indexOf('windows nt 5.0') > 0) return 'Microsoft Windows 2000';
          if (tmp.indexOf('windows nt 5.1') > 0) return 'Microsoft Windows XP';
          if (tmp.indexOf('windows nt 5.2') > 0) return 'Microsoft Windows Server 2003 or Server 2003 R2';
          if (tmp.indexOf('windows nt 6.0') > 0) return 'Microsoft Windows Vista or Server 2008';
          if (tmp.indexOf('windows nt 6.1') > 0) return 'Microsoft Windows 7 or Server 2008';
          if (tmp.indexOf('windows nt 6.2') > 0) return 'Microsoft Windows 8 or Server 2012';
          if (tmp.indexOf('windows nt 6.3') > 0) return 'Microsoft Windows 8.1 or Server 2012 R2';
          if (tmp.indexOf('windows nt 10') > 0) return 'Microsoft Windows 10 or Server 2016';

          return 'UnknownWindowsVersion';
        } else {
          if (ua.search('Linux') > -1) return 'Linux';
          if (ua.search('Macintosh') > -1) return 'Macintosh';
          if (ua.search('Mac OS X') > -1) return 'Mac OS X';

          return 'UnknownDesktopeOperationSystem';
        }
      } else {
        if(ua.match(/iPhone/) || ua.match(/iPod/) || ua.match(/iPhone/) && !window.MSStream) return 'iOS';
        if(ua.match(/BlackBerry/)) return 'BlackBerry OS';
        if(ua.match(/(Windows Phone OS|Windows CE|Windows Mobile)/)) return 'Windows Phone';
        if(ua.match(/Android/)) return 'Android';

        return 'UnknownMobileOperationgSystem';
      }
    }
}


// class for client information
class Visitor extends Sprout {
    constructor(trunk) {
        super(trunk);
        [
          new DateInfo(trunk, this._id),
          new LocationInfo(trunk, this._id),
          new DeviceInfo(trunk, this._id)
        ].forEach((rame) => {
          this.pushRame(rame);
        });
        this.name="Visitor";
    }
    // Main API
    getDateInfo() {
      return this.rames[0];
    }
    getLocationInfo() {
      return this.rames[1];
    }
    getDeviceInfo() {
      return this.rames[2];
    }
}


// class for events
class TagClick extends Leaf {
  constructor(trunk, sprout, rame, elm) {
    super(trunk,sprout, rame);
    this.date = new Date();
    this.tag = elm.target.localName;
    if (this.tag === 'a') {
        this.innerText = elm.target.innerText;
        this.href= elm.target.href;
    }
    this.id = elm.target.id;
    this.className = elm.target.className;
  }
}


// Click listener object
class ClickListener extends Rame {
  constructor(trunk, sprout, obj) {
    super(trunk, sprout);
    if(obj.type === 'click') {
      if(obj.state === 'id' && obj.value === null){
         document.onclick = (e) => {
          if(e.target.id && obj.value === null) {
              this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
          }
        };
      } else if(state === 'id' && value !== null) {
        document.onclick = (e) => {
          if(e.target.id === value) {
            this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
          }
        };
      } else if(state === 'class' && value === null) {
        document.onclick = (e) => {
          if(e.target.className) {
            this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
          }
        };
      } else if(state === 'class' && value !== null) {
        document.onclick = (e) => {
          if(e.target.className === value) {
            this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
          }
        };
      } else {
        document.onclick = (e) => {
          this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
        };
      }
    } else if (obj.type === 'inherit-click') {
        document.onclick = (e) => {
          e.path.forEach((p) => {
              if (obj.state === 'id') {
                if (obj.value === p.id) {
                  this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                }
              } else if (obj.state === 'class') {
                p.className.split(' ').forEach((c) => {
                    if (c === obj.value) {
                        this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                    }
                  });
              }
            });
        };
    }
  }
}


// Class for event storage
class EventStorage extends Sprout {
  constructor(trunk, obj) {
    super(trunk);
    this.date = new Date();
    this.url = location.hostname + location.pathname + location.search;
    if(obj.type === 'click' || obj.type === 'inherit-click') {
      this.pushRame(new ClickListener(trunk, this._id, obj));
    }
    this.name = "EventStorage";
  }
  // Main API
  getListenerList() {
    return this.rames;
  }
  getEventList(index) {
    return this.rames[index].leafs;
  }
}


// Class for sockets
class Socket extends Sprout {
  constructor(trunk, obj) {
    super(trunk._id);
    this.name = "Socket";
    this.time = new Date();
    this.connection = obj.lib(obj.url);
    this.onList = [];
    this.emitList = [];
    this.unknownEvens = [];
    if(obj.events.length !== 0 && obj.events !== undefined) {
      obj.events.forEach((socketEvent) => {
        this.pushRame(new SocketEvent(trunk, this._id, socketEvent, this.connection));
      });
    }
    this.wateringSprout();
  }
  // Main API
  wateringSprout() {
    this.rames.forEach((rame) => {
        if (rame.socketEvent === 'on') {
            this.onList.push(rame);
        } else if (rame.socketEvent === 'emit') {
            this.emitList.push(rame);
        }
      });
  }
  getEventList() {
    return this.rames;
  }
}


// Class for socket event listener
class SocketEvent extends Rame {
  constructor(trunk, sprout, obj, connect){
    super(trunk._id, sprout);
    this.date = new Date();
    this.connect = connect;
    
    let counter = 0;
    
    
    ['message',
      'socketEvent',
      'callback'].forEach((item) => {
      this[item] = obj[item];
      counter++;
      if(counter === 3) {
        if(obj.socketEvent === 'on') {
          if (this.callback !== null) {
            connect.on(this.message, (msg) => {
                this.callback(msg);
              });
          } else {
            connect.on('snd', (msg) => {
                trunk.getFormsStorage().updateForms(msg);
              });
          }
        } else {
          console.log('emit');
        }
      }
    });
  }
  // Main API
  emit(wtf) {
    this.connect.emit(this.message, this.connect.id, wtf);
  }
}


// Class for form storage
class FormStorage extends Sprout {
  constructor(trunk, obj) {
    super(trunk);
    this.name = "FormStorage";
    if(obj.forms.length === 0) {
      document.addEventListener('DOMContentLoaded',() => {
        let forms = document.getElementsByTagName('form'),
            max = forms.length,
            counter = 0,
            self = this;
        for(counter; counter < max; counter++) {
            self.pushRame(new TagForm(trunk, self._id, forms[counter]));
        }
      });
    } else if (obj.forms.length === 1) {
        let forms = document.getElementsByTagName('form'),
            max = forms.length,
            counter = 0,
            self = this;
        for(counter; counter < max; counter++) {
            self.pushRame(new TagForm(trunk, self._id, forms[counter]));
        }
    } else {
      document.addEventListener('DOMContentLoaded',() => {
        let form = null;
        obj.forms.forEach((fid) => {
          form = document.getElementById(fid);
          this.pushRame(new TagForm(trunk, self._id, form));
        });
      });
    }
  }
  getFormsList() {
    return this.rames;
  }
  updateFormDOM(form) {
    console.log(form);
  }
  updateFormStorage() {
    
  }
  updateForms(updateInfo) {
    JSON.parse(updateInfo).forEach((form) => {
        this.updateFormDOM(form);
      });
  }
}


// Class for generic tag
class GenericTag extends Rame {
  constructor(trunk, sprout, tagObj) {
    super(trunk, sprout);
    ['className',
      'id',
      'nodeName',
      'childNodes'].forEach((item) => {
      this[item] = tagObj[item];
    });
  }
}


// Class for tag form
class TagForm extends GenericTag {
  constructor(trunk, sprout, tagObj) {
    super(trunk, sprout, tagObj);
    ['method',
        'name'].forEach((item) => {
        this[item] = tagObj[item];
      });
    if(tagObj.childNodes.length !== 0) {
      tagObj.childNodes.forEach((tag) => {
        if(tag.nodeName === 'INPUT') {
          this.pushLeaf(new TagInput(trunk, sprout, this._id, tag));
        }
      });
    }
  }
  // Main API
  getInputsList() {
    return this.leafs;
  }
}


// Class for tag input
class TagInput extends Leaf {
  constructor(trunk, sprout, rame, obj) {
    super(trunk, sprout, rame);
    ['value',
      'type',
      'className',
      'id',
      'name',
      'nodeName',
      'childNodes'].forEach((item) => {
      this[item] = obj[item];
    });
    if (obj.type === 'checkbox') {
        this.checked = obj.checked;
    }
  }
  getValues() {
    this.value = document.getElementsByName(this.name)[0].value;
  }
}

