// utils
function applyChanges(lst) {
  let index = null,
      len = null;
  for (index = 0, len = lst.length; index < len; ++index) {
    applyChangeToField(lst[index]);
  }
}


function applyChangeToField(form) {
    let index = null,
        len = null;
    for(index = 0, len = form.fields.length; index < len; ++index){
      document.getElementById(form.fields[index].tagId).value = form.fields[index].value;
    }
}
