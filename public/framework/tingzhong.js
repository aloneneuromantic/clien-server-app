// (пер) - слушатели
function addInitListener(trigger) {
  let listener = new shenshengListener();
  listener.setListener('DOMContentLoaded', trigger);
  return listener;
}

function createSocket(socketLib, address) {
  let thisSocket = new shenshengSocket();
  thisSocket.connection = socketLib(address);
  thisSocket.time = new Date();
  return thisSocket;
}

function addSocketEvent(cobj, thisEvent, eventType, trigger) {
  let newEvent = new socketEvent();
  newEvent.setAttr('parent', cobj._id);
  newEvent.setAttr('event', thisEvent);
  newEvent.setAttr('callback', trigger);
  newEvent.setAttr('eventType', eventType);
  if(eventType === 'on') {
    cobj.on(newEvent.event, newEvent.callback);
  } else {
    cobj.emit('get forms', cobj.id, trigger);
  }
  return newEvent;
}
