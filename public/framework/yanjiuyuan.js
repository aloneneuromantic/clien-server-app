// исследователи
// Get form from page
function getElemForms(formId) {
  if(formId !== undefined) {
    return [
      document.getElementById(formId)
    ];
  } else {
    return document.getElementsByTagName('form');
  }
}
// Big function to build object
function buildObject(t, o) {
  let obj = null;
  switch (t) {
    case 'form':
      obj = new tagForm();
      obj.tagName = o.localName;
      obj.tagId = o.id;
      obj.classes = obj.className;
      obj.internalElements = o.childNodes;
      return obj;
    default:
      break;
  }
}
// get list forms objects
function getObjsForms(formId) {
  let tmp = null;
  if(formId === undefined) {
    tmp = getElemForms();
    if(tmp.length === 1) {
      return [
              buildObject('form', tmp[0])
            ];
    } else {
      let index = null,
          len = null,
          obj = null,
          res = [];
      for (index = 0, len = tmp.length; index < len; ++index) {
        res.push(buildObject('form', tmp[index]));
      }
      return res;
    }
  } else {
    tmp = getObjsForms(formId);
    return [
        buildObject('form', tmp[0])
    ];
  }
}
// function to get input in childNodes
function getInputInArray(childArray) {
  let res = [],
      obj = null,
      i = null;
  for (i = 0; i < childArray.length; ++i) {
    if(childArray[i].nodeName === 'INPUT') {
      obj = new tagInput();
      obj.tagName = childArray[i].localName;
      obj.tagId = childArray[i].id;
      obj.classes =childArray[i].classList;
      obj.internalElements = null;
      obj.type = childArray[i].type;
      obj.value = childArray[i].value;
      res.push(obj);
    }
  }
  return res;
}
// Add fields to form object
function getFieldsToForms(formArray) {
  let index = null,
      len = null;
  for (index = 0, len = formArray.length; index < len; ++index) {
    formArray[index].fields = getInputInArray(formArray[index].internalElements);
  }
  return formArray;
}
// final object with forms
function getFormsList() {
  return getFieldsToForms(getObjsForms());
}
