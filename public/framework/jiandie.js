// "пер" - шпион
// Function to get date & time information about coonection
function getDateInfo(wtf) {
  switch(wtf) {
    case 'open-date':
      return new Date();
    case 'timezone':
      return (new Date).getTimezoneOffset()/60;
    case 'list-all':
      return [
        (new Date()),
        ((new Date()).getTimezoneOffset()/60)
      ];
    default:
      return {
        'openDate':(new Date()),
        'timeZone':((new Date()).getTimezoneOffset()/60)
      };
  }
}
// Function to get location information about location
function getLocationInfo(wtf) {
  switch(wtf) {
    // Hash current location
    case 'hash-location':
      return location.hash;
    // Host current page
    case 'host':
      return location.hostname;
    // Current link in address string
    case 'href':
      return location.href;
    // Main link current site
    case 'origin':
      return location.origin;
    // Path name current page
    case 'pathname':
      return location.webhp;
    // Port this site
    case 'port':
      return location.port;
    // Protocol current page
    case 'protocol':
      return location.protocol;
    // Get string to pathname
    case 'search':
      return location.search;
    case 'full-url':
      return (location.hostname + location.pathname + location.search);
    case 'page-views':
      return history.length;
    case 'referrer':
      return document.referrer;
    case 'list-all':
      return [
        location.hash,
        location.hostname,
        location.href,
        location.origin,
        location.pathname,
        location.port,
        location.protocol,
        location.search,
        history.length,
        document.referrer
      ];
    default:
      return {
        'hash':location.hash,
        'host':location.hostname,
        'href':location.href,
        'origin':location.origin,
        'pathName':location.pathname,
        'port': location.port,
        'protocol': location.protocol,
        'search':location.search,
        'pageViews': history.length,
        'referrer': document.referrer
      };
  }

}

// Function for hardware detection
function getDeviceType() {
  let ua = navigator.userAgent;
  // Detection device type based on OS name
  if (ua.match(/iPhone/) ||
        ua.match(/BlackBerry/) ||
        ua.match(/(Windows Phone OS|Windows CE|Windows Mobile)/) ||
        ua.match(/Mobile/) ||
        ua.match(/(Opera Mini|IEMobile|SonyEricsson|smartphone)/)) {
      return 'mobile';
  } else if(ua.match(/iPod/) ||
              ua.match(/iPad/) ||
              ua.match(/PlayBook/) ||
              ua.match(/(GT-P1000|SGH-T849|SHW-M180S)/) ||
              ua.match(/Tablet PC/) ||
              ua.match(/(PalmOS|PalmSource| Pre\/)/) ||
              ua.match(/(Kindle)/)) {
      return 'tablet';
  } else {
      return 'desktop';
  }
}
// Function detection OS
function getDeviceOS() {
  let ua = navigator.userAgent;
  if(getDeviceType() === 'desktop') {
    if(ua.search(/Windows/) > -1) {
      let tmp = this.ua.toLowerCase();
      //
      if (tmp.indexOf('windows nt 5.0') > 0) return 'Microsoft Windows 2000';
      if (tmp.indexOf('windows nt 5.1') > 0) return 'Microsoft Windows XP';
      if (tmp.indexOf('windows nt 5.2') > 0) return 'Microsoft Windows Server 2003 or Server 2003 R2';
      if (tmp.indexOf('windows nt 6.0') > 0) return 'Microsoft Windows Vista or Server 2008';
      if (tmp.indexOf('windows nt 6.1') > 0) return 'Microsoft Windows 7 or Server 2008';
      if (tmp.indexOf('windows nt 6.2') > 0) return 'Microsoft Windows 8 or Server 2012';
      if (tmp.indexOf('windows nt 6.3') > 0) return 'Microsoft Windows 8.1 or Server 2012 R2';
      if (tmp.indexOf('windows nt 10') > 0) return 'Microsoft Windows 10 or Server 2016';

      return 'UnknownWindowsVersion';
    } else {
      if (ua.search('Linux') > -1) return 'Linux';
      if (ua.search('Macintosh') > -1) return 'Macintosh';
      if (ua.search('Mac OS X') > -1) return 'Mac OS X';

      return 'UnknownDesktopeOperationSystem';
    }
  } else {
    if(ua.match(/iPhone/) || ua.match(/iPod/) || ua.match(/iPhone/) && !window.MSStream) return 'iOS';
    if(ua.match(/BlackBerry/)) return 'BlackBerry OS';
    if(ua.match(/(Windows Phone OS|Windows CE|Windows Mobile)/)) return 'Windows Phone';
    if(ua.match(/Android/)) return 'Android';

    return 'UnknownMobileOperationgSystem';
  }
}
// Function detection real name of browser
function detectRealBrowserName() {
  let ua = navigator.userAgent;
  if(ua.search(/MSIE/) > -1) return 'InternetExplorer';
  if(ua.search(/Trident/) > -1) return 'InternetExplorer(Trident)';
  if(ua.search(/OPR/) >-1) return 'NewOpera';
  if(ua.search(/Yowser/) > -1 ) return 'YandexBrowser';
  if(ua.search(/UBrowser/) > -1) return 'UCBrowser';
  if(ua.search(/SeaMonkey/) > -1) return 'SeaMonkey';
  if(ua.search(/Iceweasel/) > -1) return 'IceWeasel';
  if(ua.search(/Opera/) > -1) return 'OldOpera';
  if(ua.search(/Firefox/) > -1) return 'Firefox';
  if(ua.search(/Vivaldi/) > -1) return 'Vivaldi';
  if(ua.search(/Edge/) > -1) return 'Edge';
  if(ua.search(/Safari/) > -1 && navigator.vendor.indexOf('Apple') >-1 && ua && ua.match('CriOS')) return 'Safari';
  if(ua.search(/Konqueror/) > -1) return 'Konqueror';
  if(ua.search(/Chrome/) > -1 ) return 'GoogleChrome';

  return 'UnknownBrowser';
}
// Function detection browser info
function getBrowserInfo(wtf) {
  switch(wtf){
    // Real browser name
    case 'name':
      return detectRealBrowserName();
    case 'app-name':
      return navigator.appName;
    case 'version':
      return navigator.appVersion;
    case 'user-agent':
      return navigator.userAgent;
    case 'engine':
      return navigator.product;
    case 'language':
      return navigator.language;
    case 'language-list':
      return navigator.languages;
    case 'is-online':
      return navigator.onLine;
    case 'browser-platform':
      return navigator.platform;
    case 'java-enable':
      return navigator.javaEnabled();
    case 'cookies-enable':
      return navigator.cookieEnabled;
    case 'cookies-first':
      return document.cookie;
    case 'cookies-second':
      return decodeURIComponent(document.cookie.split(";"));
    case 'local-storage':
      return localStorage;
    case 'list-all':
      return [
        detectRealBrowserName(),
        navigator.appName,
        navigator.appVersion,
        navigator.userAgent,
        navigator.product,
        navigator.language,
        navigator.languages,
        navigator.onLine,
        navigator.platform,
        navigator.javaEnabled(),
        navigator.cookieEnabled,
        document.cookie,
        decodeURIComponent(document.cookie.split(";")),
        localStorage
      ];
    default:
      return {
        'name': detectRealBrowserName(),
        'app-name': navigator.appName,
        'version': navigator.appVersion,
        'user-agent': navigator.userAgent,
        'engine': navigator.product,
        'language': navigator.language,
        'language-list': navigator.languages,
        'is-online': navigator.onLine,
        'browser-platform': navigator.platform,
        'java-enable': navigator.javaEnabled(),
        'cookies-enable': navigator.cookieEnabled,
        'cookies-first': document.cookie,
        'cookies-second': decodeURIComponent(document.cookie.split(";")),
        'local-storage': localStorage
      };
  }
}
// Function get screen information
function getScreenInfo(wtf) {
  switch(wtf){
    case 'screen-width':
      return screen.width;
    case 'screen-height':
      return screen.height;
    case 'document-width':
      return document.width;
    case 'document-height':
      return document.height;
    case 'inner-width':
      return innerWidth;
    case 'inner-height':
      return innerHeight;
    case 'available-width':
      return screen.availWidth;
    case 'available-height':
      return screen.availHeight;
    case 'depth-color':
      return screen.colorDepth;
    case 'pixel-depth':
      return screen.pixelDepth;
    case 'list-all':
      return [
        screen.width,
        screen.height,
        document.width,
        document.height,
        innerWidth,
        innerHeight,
        screen.availWidth,
        screen.availHeight,
        screen.colorDepth,
        screen.pixelDepth
      ];
    default:
      return {
        'screen-width': screen.width,
        'screen-height': screen.height,
        'document-width': document.width,
        'document-height': document.height,
        'inner-width': innerWidth,
        'inner-height': innerHeight,
        'available-width': screen.availWidth,
        'available-height': screen.availHeight,
        'depth-color': screen.colorDepth,
        'pixel-depth': screen.pixelDepth
      };
  }
}
// Function to test
function getClientInfo() {
  return {
    date: getDateInfo(),
    location: getLocationInfo(),
    deviceType: getDeviceType(),
    os: getDeviceOS(),
    browser: getBrowserInfo(),
    screen: getScreenInfo()
  };
}
// functionn add event to list
function addEvent(lst,e) {
  lst.push({
    tag: e.target.localName,
    id: e.target.id,
    className: e.target.className
  });
}
//Function for create listener to event
function listenClicks(state, value) {
  if(state === 'id' && value === null){
     document.onclick = function(e) {
       if(e.target.id && value === null){
         addEvent(e);
       }
     }
  } else if(state === 'id' && value !== null){
    document.onclick = function(e) {
      if(e.target.id === value){
        addEvent(e);
      }
    }
  } else if(state === 'class' && value === null) {
    document.onclick = function(e) {
      if(e.target.className) {
        addEvent(e);
      }
    }
  } else if(state === 'class' && value !== null) {
    document.onclick = function(e) {
      if(e.target.className === value) {
        addEvent(e);
      }
    }
  } else {
    document.onclick = function(e) {
      addEvent(e);
    }
  }
}
